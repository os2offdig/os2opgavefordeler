#!/bin/bash

# Shutdown any running docker containers
docker-compose down

# build the http-proxy
(cd docker/httpproxy; docker build -t httpproxy .)

# build the frontend
(cd ./sources/frontend/src/main/webapp; gulp clean-build-app-dev)
cp -r ./sources/frontend/src/main/webapp/dist.dev/* ./docker/frontend/files
(cd ./docker/frontend; docker build -t frontend .)

# build the backend
(cd ./sources/TopicRouter/; mvn clean install -DskipTests)
cp ./sources/TopicRouter/target/TopicRouter.war ./docker/backend/files/
(cd docker/backend; docker build -t backend .)

###
### Start Liquibase section - enable these commands to create the tables in the MySQL database
###

#docker-compose up -d mysql
#echo "Waiting 15sec for mysql to start"
#sleep 15
#(cd ./sources/TopicRouter/; mvn liquibase:updateSQL)
#echo -e "USE os2opgavefordeler;\n$(cat ./sources/TopicRouter/target/liquibase/migrate.sql)" > ./sources/TopicRouter/target/liquibase/migrate.sql
#mysql -u os2 -pTest1234 -h 127.0.0.1 --port 6666 < ./sources/TopicRouter/target/liquibase/migrate.sql

###
### End Liquibase section
###

docker-compose up -d
