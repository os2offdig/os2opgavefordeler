There are two components that must be build, the backend and the frontend

The backend is a java application, which is build using Maven - so ensure that you have both Java 8 and Maven 3 installed before attempting to build the application.

Go to sources/TopicRouter and run

$ mvn clean install

this will build the binary and run all tests... to speed up building, the tests can by skipped with this command

$ mvn clean install -Dmaven.test.skip=true

The frontend is an Angular application, and the following steps must be followed to build the application

1. Install npm:  sudo apt install npm
2. Install node: sudo apt install nodejs-legacy
3. Install bower: sudo npm install -g bower
4. Install gulp: sudo apt install gulp
5. go to /src/main/webapp
6. Run bower install: bower install
7. Run "npm install" to install dependencies
8. Run "gulp clean-build-app-dev" in order to compile the code. Output will be in "dist.dev" folder. You can copy that to web server.

the "dev" build is used for production - cleanup of the code so there are no environment specific builds is on the backlog

Everything is tied together using Docker - there is a script on the root folder called start.sh which can be used to perform a full build, but the steps are roughly

1) compile the backend and frontend

2) build a docker image for the frontend and another for the backend using the Dockerfile scripts found in the docker folder

3) start everything using the docker-compose.yml file found in the root folder, note that two other contains are started these are

- MySQL for the database
- Apache httpd as a reverse proxy (to remove any CORS issues)

First time the application runs, it will fail, as the database is empty, so we need to populate the database and restart the application

$ docker-compose up -d

3a) populat the database (once)

$ cd ./sources/TopicRouter/
$ mvn liquibase:updateSQL
$ mysql -u os2 -pTest1234 -h 127.0.0.1 --port 6666 < target/liquibase/migrate.sql

3b) restart the application using docker-compose (once)

$ docker-compose down
$ docker-compose up -d

4) bootstrap the application with some development data for testing purposes

GET https://localhost:8443/TopicRouter/bootstrap

Once completed, login is possible

Note that the SAML setup is found in the oiosaml-config folder, and it is pre-configured to run against Digital Identitys test AD FS

