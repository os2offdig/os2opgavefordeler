--  *********************************************************************
--  Update Database Script
--  *********************************************************************
--  Change Log: src/main/resources/db/migration/changelog-master.xml
--  Ran at: 4/18/18 8:08 AM
--  Against: os2@172.19.0.1@jdbc:mysql://127.0.0.1:6666/os2opgavefordeler
--  Liquibase version: 3.0.5
--  *********************************************************************

--  Create Database Lock Table
CREATE TABLE DATABASECHANGELOGLOCK (ID INT NOT NULL, LOCKED BIT(1) NOT NULL, LOCKGRANTED datetime NULL, LOCKEDBY VARCHAR(255) NULL, CONSTRAINT PK_DATABASECHANGELOGLOCK PRIMARY KEY (ID));

--  Initialize Database Lock Table
DELETE FROM DATABASECHANGELOGLOCK;

INSERT INTO DATABASECHANGELOGLOCK (ID, LOCKED) VALUES (1, 0);

--  Lock Database
--  Create Database Change Log Table
CREATE TABLE DATABASECHANGELOG (ID VARCHAR(63) NOT NULL, AUTHOR VARCHAR(63) NOT NULL, FILENAME VARCHAR(200) NOT NULL, DATEEXECUTED datetime NOT NULL, ORDEREXECUTED INT NOT NULL, EXECTYPE VARCHAR(10) NOT NULL, MD5SUM VARCHAR(35) NULL, DESCRIPTION VARCHAR(255) NULL, COMMENTS VARCHAR(255) NULL, TAG VARCHAR(255) NULL, LIQUIBASE VARCHAR(20) NULL, CONSTRAINT PK_DATABASECHANGELOG PRIMARY KEY (ID, AUTHOR, FILENAME));

--  Changeset src/main/resources/db/migration/V2_create_distributionassignment.xml::create_table_distributionassignment::Kresten::(Checksum: 7:40580fb472cdadea81afcd0015b35503)
CREATE TABLE distributionrule (id BIGINT AUTO_INCREMENT NOT NULL, assignedemp BIGINT NULL, assignedorg_id BIGINT NULL, kle_id BIGINT NULL, municipality_id BIGINT NULL, parent_id BIGINT NULL, responsibleorg_id BIGINT NULL, CONSTRAINT PK_DISTRIBUTIONRULE PRIMARY KEY (id), UNIQUE (id));

CREATE TABLE identityprovider (id BIGINT AUTO_INCREMENT NOT NULL, clientid LONGTEXT NOT NULL, clientsecret LONGTEXT NOT NULL, idpurl LONGTEXT NOT NULL, name LONGTEXT NOT NULL, CONSTRAINT PK_IDENTITYPROVIDER PRIMARY KEY (id), UNIQUE (id));

CREATE TABLE kle (id BIGINT AUTO_INCREMENT NOT NULL, datecreated date NOT NULL, description LONGTEXT NOT NULL, number VARCHAR(255) NOT NULL, title VARCHAR(255) NOT NULL, parent_id BIGINT NULL, CONSTRAINT PK_KLE PRIMARY KEY (id), UNIQUE (id));

CREATE TABLE municipality (id BIGINT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, active BIT(1) DEFAULT 1 NULL, CONSTRAINT PK_MUNICIPALITY PRIMARY KEY (id), UNIQUE (id));

CREATE TABLE orgunit (id BIGINT AUTO_INCREMENT NOT NULL, businesskey VARCHAR(255) NOT NULL, email VARCHAR(255) NULL, esdhid VARCHAR(255) NULL, esdhlabel VARCHAR(255) NULL, isactive BIT(1) DEFAULT 0 NULL, name VARCHAR(255) NULL, phone VARCHAR(255) NULL, manager_id BIGINT NULL, municipality_id BIGINT NULL, parent_id BIGINT NULL, CONSTRAINT PK_ORGUNIT PRIMARY KEY (id), UNIQUE (id));

CREATE TABLE role (id BIGINT AUTO_INCREMENT NOT NULL, admin BIT(1) NOT NULL, manager BIT(1) NOT NULL, municipalityadmin BIT(1) NOT NULL, name VARCHAR(255) NOT NULL, substitute BIT(1) NOT NULL, postDistributor TINYINT(1) DEFAULT 0 NOT NULL, employment_id BIGINT NULL, owner_id BIGINT NULL, CONSTRAINT PK_ROLE PRIMARY KEY (id), UNIQUE (id));

CREATE TABLE tr_user (id BIGINT AUTO_INCREMENT NOT NULL, email VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, municipality_id BIGINT NULL, settings_id BIGINT NULL, CONSTRAINT PK_TR_USER PRIMARY KEY (id), UNIQUE (id));

CREATE TABLE employment (id BIGINT AUTO_INCREMENT NOT NULL, businesskey VARCHAR(255) NULL, email VARCHAR(255) NULL, esdhid VARCHAR(255) NULL, esdhlabel VARCHAR(255) NULL, initials VARCHAR(255) NULL, isactive BIT(1) NULL, jobtitle VARCHAR(255) NULL, name VARCHAR(255) NULL, phone VARCHAR(255) NULL, employedin_id BIGINT NULL, municipality_id BIGINT NULL, CONSTRAINT PK_EMPLOYMENT PRIMARY KEY (id), UNIQUE (id));

CREATE TABLE usersettings (id BIGINT AUTO_INCREMENT NOT NULL, scope VARCHAR(255) NOT NULL, showexpandedorg BIT(1) DEFAULT 1 NOT NULL, showresponsible BIT(1) DEFAULT 1 NOT NULL, userid BIGINT NULL, settings_id BIGINT NULL, CONSTRAINT PK_USERSETTINGS PRIMARY KEY (id), UNIQUE (id));

CREATE TABLE hibernate_sequence (sequence_name VARCHAR(255) NULL, last_value BIGINT NULL, start_value BIGINT NULL, increment_by BIGINT NULL, max_value BIGINT NULL, min_value BIGINT NULL, cache_value BIGINT NULL, log_cnt BIGINT NULL, is_cycled BIT(1) NULL, is_called BIT(1) NULL);

ALTER TABLE orgunit ADD CONSTRAINT orgunit2municipality FOREIGN KEY (municipality_id) REFERENCES municipality (id);

ALTER TABLE employment ADD CONSTRAINT employment2orgunit FOREIGN KEY (employedin_id) REFERENCES orgunit (id);

ALTER TABLE role ADD CONSTRAINT role2employment FOREIGN KEY (employment_id) REFERENCES employment (id);

ALTER TABLE distributionrule ADD CONSTRAINT distributionrule2orgunit FOREIGN KEY (responsibleorg_id) REFERENCES orgunit (id);

ALTER TABLE orgunit ADD CONSTRAINT orgunit2orgunit FOREIGN KEY (parent_id) REFERENCES orgunit (id);

ALTER TABLE distributionrule ADD CONSTRAINT distributionrule2kle FOREIGN KEY (kle_id) REFERENCES kle (id);

ALTER TABLE tr_user ADD CONSTRAINT tr_user2municipality FOREIGN KEY (municipality_id) REFERENCES municipality (id);

ALTER TABLE kle ADD CONSTRAINT kle2kle FOREIGN KEY (parent_id) REFERENCES kle (id);

ALTER TABLE role ADD CONSTRAINT role2tr_user FOREIGN KEY (owner_id) REFERENCES tr_user (id);

ALTER TABLE distributionrule ADD CONSTRAINT distributionrule2orgunit4assigned FOREIGN KEY (assignedorg_id) REFERENCES orgunit (id);

ALTER TABLE orgunit ADD CONSTRAINT orgunit2employment FOREIGN KEY (manager_id) REFERENCES employment (id);

ALTER TABLE employment ADD CONSTRAINT employment2municipality FOREIGN KEY (municipality_id) REFERENCES municipality (id);

ALTER TABLE distributionrule ADD CONSTRAINT distributionrule2distributionrule FOREIGN KEY (parent_id) REFERENCES distributionrule (id);

ALTER TABLE distributionrule ADD CONSTRAINT distributionrule2municipality FOREIGN KEY (municipality_id) REFERENCES municipality (id);

INSERT INTO DATABASECHANGELOG (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, LIQUIBASE) VALUES ('create_table_distributionassignment', 'Kresten', 'src/main/resources/db/migration/V2_create_distributionassignment.xml', NOW(), 1, '7:40580fb472cdadea81afcd0015b35503', 'Create Table (x10), Adds a foreign key constraint to an existing column (x14)', '', 'EXECUTED', '3.0.5');

--  Changeset src/main/resources/db/migration/V3_add_token_to_municipality.xml::create_table_distributionassignment::Kresten::(Checksum: 7:eaff5fadaada1c785464af1f9d0e7602)
ALTER TABLE municipality ADD token VARCHAR(255) NULL;

INSERT INTO DATABASECHANGELOG (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, LIQUIBASE) VALUES ('create_table_distributionassignment', 'Kresten', 'src/main/resources/db/migration/V3_add_token_to_municipality.xml', NOW(), 2, '7:eaff5fadaada1c785464af1f9d0e7602', 'Adds a new column to an existing table', '', 'EXECUTED', '3.0.5');

--  Changeset src/main/resources/db/migration/V4_add_municipality_to_kle.xml::create_table_distributionassignment::Kresten::(Checksum: 7:ad4aae088ca33d2b9a3a4d4b1372ccda)
ALTER TABLE kle ADD municipality_id BIGINT NULL;

INSERT INTO DATABASECHANGELOG (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, LIQUIBASE) VALUES ('create_table_distributionassignment', 'Kresten', 'src/main/resources/db/migration/V4_add_municipality_to_kle.xml', NOW(), 3, '7:ad4aae088ca33d2b9a3a4d4b1372ccda', 'Adds a new column to an existing table', '', 'EXECUTED', '3.0.5');

--  Changeset src/main/resources/db/migration/V5_add_distribution_rule_filter.xml::create_table_distributionassignment::Kresten::(Checksum: 7:7651855188cf49daad297ae07b03a419)
CREATE TABLE distributionrulefilter (id BIGINT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, distributionrule_id BIGINT NOT NULL, assignedorg_id BIGINT NULL, assignedemp BIGINT NULL, DTYPE VARCHAR(255) NOT NULL, days VARCHAR(255) NULL, months VARCHAR(255) NULL, CONSTRAINT PK_DISTRIBUTIONRULEFILTER PRIMARY KEY (id), UNIQUE (id));

ALTER TABLE distributionrulefilter ADD CONSTRAINT distributionrulefilter2assignedOrg FOREIGN KEY (assignedorg_id) REFERENCES orgunit (id);

ALTER TABLE distributionrulefilter ADD CONSTRAINT distributionrulefilter2assignedemp FOREIGN KEY (assignedemp) REFERENCES employment (id);

CREATE TABLE distributionrule_distributionrulefilter (distributionrule_id BIGINT NOT NULL, filters_id BIGINT NOT NULL, CONSTRAINT PK_DISTRIBUTIONRULE_DISTRIBUTIONRULEFILTER PRIMARY KEY (distributionrule_id, filters_id));

ALTER TABLE distributionrule_distributionrulefilter ADD CONSTRAINT distributionrule_distributionrulefilter2DistributionRule FOREIGN KEY (distributionrule_id) REFERENCES distributionrule (id);

ALTER TABLE distributionrule_distributionrulefilter ADD CONSTRAINT distributionrule_distributionrulefilter2filters FOREIGN KEY (filters_id) REFERENCES distributionrulefilter (id);

CREATE TABLE distributionrulefiltername (id BIGINT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, default_name BIT(1) DEFAULT 1 NOT NULL, municipality_id BIGINT NULL, CONSTRAINT PK_DISTRIBUTIONRULEFILTERNAME PRIMARY KEY (id), UNIQUE (id));

ALTER TABLE distributionrulefiltername ADD CONSTRAINT distributionrulefiltername2municipality FOREIGN KEY (municipality_id) REFERENCES municipality (id);

INSERT INTO DATABASECHANGELOG (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, LIQUIBASE) VALUES ('create_table_distributionassignment', 'Kresten', 'src/main/resources/db/migration/V5_add_distribution_rule_filter.xml', NOW(), 4, '7:7651855188cf49daad297ae07b03a419', 'Create Table, Adds a foreign key constraint to an existing column (x2), Create Table, Adds a foreign key constraint to an existing column (x2), Create Table, Adds a foreign key constraint to an existing column', '', 'EXECUTED', '3.0.5');

--  Changeset src/main/resources/db/migration/V6_create_municipality_to_user_relation.xml::create_municipality_to_user_relation::Kresten::(Checksum: 7:786fe9eca55bdeacc3a68254c6682960)
CREATE TABLE municipality_tr_user (municipality_id BIGINT NOT NULL, users_id BIGINT NOT NULL, CONSTRAINT PK_MUNICIPALITY_TR_USER PRIMARY KEY (municipality_id, users_id));

ALTER TABLE municipality_tr_user ADD CONSTRAINT `municipality_tr_user->tr_user` FOREIGN KEY (users_id) REFERENCES tr_user (id);

ALTER TABLE municipality_tr_user ADD CONSTRAINT `municipality_tr_user->municipality` FOREIGN KEY (municipality_id) REFERENCES municipality (id);

INSERT INTO DATABASECHANGELOG (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, LIQUIBASE) VALUES ('create_municipality_to_user_relation', 'Kresten', 'src/main/resources/db/migration/V6_create_municipality_to_user_relation.xml', NOW(), 5, '7:786fe9eca55bdeacc3a68254c6682960', 'Create Table, Adds a foreign key constraint to an existing column (x2)', '', 'EXECUTED', '3.0.5');

--  Changeset src/main/resources/db/migration/V7_create_text_distribution_filter.xml::create_text_distribution_filter::Kresten::(Checksum: 7:6b8ff6bfef4a7d91a7992428680b6408)
ALTER TABLE distributionrulefilter ADD text VARCHAR(255) NULL;

INSERT INTO DATABASECHANGELOG (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, LIQUIBASE) VALUES ('create_text_distribution_filter', 'Kresten', 'src/main/resources/db/migration/V7_create_text_distribution_filter.xml', NOW(), 6, '7:6b8ff6bfef4a7d91a7992428680b6408', 'Adds a new column to an existing table', '', 'EXECUTED', '3.0.5');

--  Changeset src/main/resources/db/migration/V8_distributionrule_add_delete_on_parent_delete.xml::create_text_distribution_filter::Kresten::(Checksum: 7:c4f711347fb7b34f97d0277ac082a00b)
ALTER TABLE distributionrule DROP FOREIGN KEY distributionrule2distributionrule;

ALTER TABLE distributionrule ADD CONSTRAINT distributionrule2distributionrule FOREIGN KEY (parent_id) REFERENCES distributionrule (id) ON DELETE CASCADE;

INSERT INTO DATABASECHANGELOG (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, LIQUIBASE) VALUES ('create_text_distribution_filter', 'Kresten', 'src/main/resources/db/migration/V8_distributionrule_add_delete_on_parent_delete.xml', NOW(), 7, '7:c4f711347fb7b34f97d0277ac082a00b', 'Drops an existing foreign key, Adds a foreign key constraint to an existing column', '', 'EXECUTED', '3.0.5');

--  Changeset src/main/resources/db/migration/V9_create_auditlog.xml::create_table_auditlog::Rune::(Checksum: 7:ae63d01246449c979567147519996acd)
CREATE TABLE auditlog (id BIGINT AUTO_INCREMENT NOT NULL, eventtime date NULL, kle VARCHAR(255) NULL, userName VARCHAR(255) NULL, operation VARCHAR(255) NULL, eventtype VARCHAR(255) NULL, eventdata VARCHAR(255) NULL, orgunit VARCHAR(255) NULL, employment VARCHAR(255) NULL, municipality_id BIGINT NULL, CONSTRAINT PK_AUDITLOG PRIMARY KEY (id), UNIQUE (id));

INSERT INTO DATABASECHANGELOG (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, LIQUIBASE) VALUES ('create_table_auditlog', 'Rune', 'src/main/resources/db/migration/V9_create_auditlog.xml', NOW(), 8, '7:ae63d01246449c979567147519996acd', 'Create Table', '', 'EXECUTED', '3.0.5');

--  Changeset src/main/resources/db/migration/V10_add_kleassigner_role.xml::add_kleassigner_role::Brian::(Checksum: 7:2679405364353f654cc460a0bc58abef)
ALTER TABLE role ADD kleAssigner TINYINT(1) NOT NULL DEFAULT '0';

INSERT INTO DATABASECHANGELOG (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, LIQUIBASE) VALUES ('add_kleassigner_role', 'Brian', 'src/main/resources/db/migration/V10_add_kleassigner_role.xml', NOW(), 9, '7:2679405364353f654cc460a0bc58abef', 'Adds a new column to an existing table', '', 'EXECUTED', '3.0.5');

--  Changeset src/main/resources/db/migration/V11_extend_orgunit_with_kles.xml::add_kle_ou_mapping::Piotr::(Checksum: 7:d8ad8a0692a14af51a5bc4313984ec67)
CREATE TABLE ou_kle_mapping (id BIGINT AUTO_INCREMENT NOT NULL, kle_id BIGINT NULL, ou_id BIGINT NULL, assignment_type VARCHAR(255) NOT NULL, CONSTRAINT PK_OU_KLE_MAPPING PRIMARY KEY (id), UNIQUE (id));

ALTER TABLE ou_kle_mapping ADD CONSTRAINT kle2ou FOREIGN KEY (ou_id) REFERENCES orgunit (id);

ALTER TABLE ou_kle_mapping ADD CONSTRAINT ou2kle FOREIGN KEY (kle_id) REFERENCES kle (id);

INSERT INTO DATABASECHANGELOG (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, LIQUIBASE) VALUES ('add_kle_ou_mapping', 'Piotr', 'src/main/resources/db/migration/V11_extend_orgunit_with_kles.xml', NOW(), 10, '7:d8ad8a0692a14af51a5bc4313984ec67', 'Create Table, Adds a foreign key constraint to an existing column (x2)', '', 'EXECUTED', '3.0.5');

--  Changeset src/main/resources/db/migration/V12_add_pnumber_to_orgunit.xml::add_pnumber_to_orgunit::Piotr::(Checksum: 7:55aaef7ca51973ae99c8ce779c0c04ea)
ALTER TABLE orgunit ADD pnumber VARCHAR(255) NULL;

INSERT INTO DATABASECHANGELOG (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, LIQUIBASE) VALUES ('add_pnumber_to_orgunit', 'Piotr', 'src/main/resources/db/migration/V12_add_pnumber_to_orgunit.xml', NOW(), 11, '7:55aaef7ca51973ae99c8ce779c0c04ea', 'Adds a new column to an existing table', '', 'EXECUTED', '3.0.5');

--  Changeset src/main/resources/db/migration/V13_jira_feed.xml::add_pnumber_to_orgunit::Piotr::(Checksum: 7:c56409a467469c27a3bda6381a97e076)
CREATE TABLE jira_sprint (id BIGINT AUTO_INCREMENT NOT NULL, state VARCHAR(64) NOT NULL, name VARCHAR(255) NOT NULL, start_date date NULL, end_date date NULL, CONSTRAINT PK_JIRA_SPRINT PRIMARY KEY (id), UNIQUE (id));

CREATE TABLE jira_task (id BIGINT AUTO_INCREMENT NOT NULL, issue_key VARCHAR(64) NOT NULL, summary VARCHAR(255) NOT NULL, jira_sprint_id BIGINT NOT NULL, CONSTRAINT PK_JIRA_TASK PRIMARY KEY (id), UNIQUE (id));

ALTER TABLE jira_task ADD CONSTRAINT jira_task2jira_sprint FOREIGN KEY (jira_sprint_id) REFERENCES jira_sprint (id);

INSERT INTO DATABASECHANGELOG (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, LIQUIBASE) VALUES ('add_pnumber_to_orgunit', 'Piotr', 'src/main/resources/db/migration/V13_jira_feed.xml', NOW(), 12, '7:c56409a467469c27a3bda6381a97e076', 'Create Table (x2), Adds a foreign key constraint to an existing column', '', 'EXECUTED', '3.0.5');

--  Changeset src/main/resources/db/migration/V14_ou_foreign_keys.xml::add_foreignkeys_to_orgunit::bsg::(Checksum: 7:f26caea8d0c2bb5904b73ab82f093bb0)
CREATE TABLE ou_foreign_key (ou_id BIGINT NULL, name VARCHAR(255) NOT NULL, value VARCHAR(255) NOT NULL);

ALTER TABLE ou_foreign_key ADD CONSTRAINT foreignkey2ou FOREIGN KEY (ou_id) REFERENCES orgunit (id);

INSERT INTO DATABASECHANGELOG (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, LIQUIBASE) VALUES ('add_foreignkeys_to_orgunit', 'bsg', 'src/main/resources/db/migration/V14_ou_foreign_keys.xml', NOW(), 13, '7:f26caea8d0c2bb5904b73ab82f093bb0', 'Create Table, Adds a foreign key constraint to an existing column', '', 'EXECUTED', '3.0.5');

--  Changeset src/main/resources/db/migration/V15_xxx (manually added)

ALTER TABLE distributionrulefilter ADD priority INT NOT NULL;

INSERT INTO DATABASECHANGELOG (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, LIQUIBASE) VALUES ('add_order_to_distributionrulefilter', 'psu', 'src/main/resources/db/migration/V15_add_order_to_distributionrulefilter.xml', NOW(), 14, '7:c1dcc9cd0a6fe4fac467f0fc543e8c0d', 'Adds a new column to an existing table', '', 'EXECUTED', '3.0.5');

