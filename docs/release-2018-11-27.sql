USE xxxx;

ALTER TABLE distributionrulefilter ADD priority INT NOT NULL;

INSERT INTO DATABASECHANGELOG (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, LIQUIBASE) VALUES ('add_order_to_distributionrulefilter', 'psu', 'src/main/resources/db/migration/V15_add_order_to_distributionrulefilter.xml', NOW(), 14, '7:c1dcc9cd0a6fe4fac467f0fc543e8c0d', 'Adds a new column to an existing table', '', 'EXECUTED', '3.0.5');
