(function () {
	'use strict';

	angular.module('topicRouter').controller('LoginCtrl', LoginCtrl);

	LoginCtrl.$inject = ['$scope', 'topicRouterApi', 'bootstrapService', 'serverUrl', '$location', '$q'];

	function LoginCtrl($scope, topicRouterApi, bootstrapService, serverUrl, $location, $q) {
		/* jshint validthis:true */
		$scope.providers = [];
		$scope.dev = false;

		$scope.bootstrap = function() { bootstrapService.bootstrap(); };
		$scope.email = "";

		$scope.alerts = [];
		$scope.addAlert = addAlert;
		$scope.closeAlert = closeAlert;

		// $scope.buildRules = function() { bootstrapService.buildRules() };

		activate();

		function activate() {
			$scope.loginUrl = serverUrl + "/login";
			getJIRAFeed();
		}

		function getJIRAFeed(){
			var deferred = $q.defer();
			var jiraFeed = topicRouterApi.getJIRAFeed().then(function (data) {
				if(data){
					$scope.activeSprints = data.activeSprints;
					$scope.closedSprints = data.closedSprints;
					$scope.browseUrl = data.browseUrl;
				}
				deferred.resolve();
			});
			return deferred.promise;
		}

		function addAlert(alert) {
			$scope.alerts.push(alert);
		}

		function closeAlert(index) {
			$scope.alerts.splice(index, 1);
		}

		var employeeError = $location.search().employeeError;

		if (employeeError=='true') {
			addAlert({
				msg: 'Der er ingen ansættelse knyttet til din brugerkonto.',
				type: 'danger'
			});
		}

	}
})();
