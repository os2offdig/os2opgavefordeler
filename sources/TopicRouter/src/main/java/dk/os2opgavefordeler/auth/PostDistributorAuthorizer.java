package dk.os2opgavefordeler.auth;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.apache.deltaspike.security.api.authorization.Secures;
import org.slf4j.Logger;

@ApplicationScoped
public class PostDistributorAuthorizer {

	@Inject
	private AuthService authService;

	@Inject
	Logger logger;

	@Secures
	@PostDistributorRequired
	public boolean doPostDistributorCheck() throws Exception {
		// Admins can do the same as post distributors
		return authService.isPostDistributor() || authService.isMunicipalityAdmin();
	}
}
