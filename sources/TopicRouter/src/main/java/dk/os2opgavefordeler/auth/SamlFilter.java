package dk.os2opgavefordeler.auth;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.slf4j.Logger;

import dk.itst.oiosaml.sp.UserAssertion;
import dk.itst.oiosaml.sp.UserAssertionHolder;
import dk.itst.oiosaml.sp.UserAttribute;
import dk.os2opgavefordeler.auth.openid.OpenIdUserFactory;
import dk.os2opgavefordeler.model.PrivilegeList;
import dk.os2opgavefordeler.model.PrivilegeList.PrivilegeGroup;
import dk.os2opgavefordeler.model.Role;
import dk.os2opgavefordeler.model.User;
import dk.os2opgavefordeler.repository.UserRepository;
import dk.os2opgavefordeler.service.UserService;

@WebFilter
public class SamlFilter implements Filter {
	// TODO: this is a dead role, and should be removed (from all the code)
	// private static final String ROLE_ADMIN = "http://os2opgavefordeler.dk/admin";
	private static final String ROLE_KLEASSIGNER = "http://os2opgavefordeler.dk/kle-assigner";
	private static final String ROLE_ADMIN = "http://os2opgavefordeler.dk/municipality-admin";
	private static final String ROLE_POSTDISTRIBUTOR = "http://os2opgavefordeler.dk/post-distributor";

	@Inject
	private AuthService authService;

	@Inject
	private UserRepository userRepository;
	
	@Inject
	private UserService userService;

	@Inject
	private Logger logger;

	@Inject
	private OpenIdUserFactory openIdUserFactory;

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
		UserAssertion ua = UserAssertionHolder.get();

		// run through this code only once, after a successful SAML login
		if (ua != null && authService.currentUser() == null) {
			UserAttribute emailAttribute = ua.getAttribute("email");
			UserAttribute simpleRolesAttribute = ua.getAttribute("roles");
			UserAttribute rolesAttribute = ua.getAttribute("dk:gov:saml:attribute:Privileges_intermediate");
			
			// email is a required attribute - otherwise we will not log the user in
			if (emailAttribute != null) {
				String email = emailAttribute.getValue();

				List<String> roles = new ArrayList<>();

				if (rolesAttribute != null) {
					try {
						roles = extractRolesFromOioBpp(rolesAttribute.getValue());
					}
					catch (Exception ex) {
						logger.warn("Unable to parse OIOBPP: ", ex);
					}
				}
				else if (simpleRolesAttribute != null && simpleRolesAttribute.getValues() != null) {
					roles = simpleRolesAttribute.getValues();
				}

				User user = null;
				try {
					// quick check to see if the user exists in the database will throw exception if not
					user = userRepository.findByEmailIgnoreCase(email);
				}
				catch (Exception ex) {
					logger.warn("Failed to authenticate user with email: " + email + ", because: " + ex.getMessage());
				}

				if (user == null) { // No user found try to create new user
					try {
						// will throw exception if no employment found for given email
						user = openIdUserFactory.createUserFromOpenIdEmail(email);
					}
					catch (RuntimeException e) {
						logger.error("Login failed: " + e.getMessage());
						((HttpServletResponse) servletResponse).sendRedirect("/#/login?employeeError=true");
						return;
					}
				}

				updateUserRoles(user, roles);

				authService.authenticateAs(email);
				logger.info("Logged in as: " + user.getName());
			}
			else {
				logger.warn("No email attribute supplied in token - rejecting user!");
			}
		}

		filterChain.doFilter(servletRequest, servletResponse);
	}

	private void updateUserRoles(User user, List<String> roles) {
		List<Role> ownRoles = user.getRoles().stream().filter(r -> r.getOwner().equals(user)).collect(Collectors.toList());
		for (Role role : ownRoles) {
			role.setKleAssigner(roles.stream().anyMatch(r -> r.equals(ROLE_KLEASSIGNER)));
			role.setMunicipalityAdmin(roles.stream().anyMatch(r -> r.equals(ROLE_ADMIN)));
			role.setPostDistributor(roles.stream().anyMatch(r -> r.equals(ROLE_POSTDISTRIBUTOR)));
		}
	}

	private List<String> extractRolesFromOioBpp(String input) throws JAXBException {
		List<String> result = new ArrayList<>();
		byte[] oiobppRawBytes = Base64.getDecoder().decode(input);

		JAXBContext jaxbContext = JAXBContext.newInstance(PrivilegeList.class);
		Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
		Reader reader = new InputStreamReader(new ByteArrayInputStream(oiobppRawBytes));
		PrivilegeList oioBPP = (PrivilegeList) unmarshaller.unmarshal(reader);

		for (PrivilegeGroup privilegeGroup : oioBPP.getPrivilegeGroup()) {
			result.add(privilegeGroup.getPrivilege());
		}

		return result;
	}

	@Override
	public void init(FilterConfig config) throws ServletException {
		;
	}

	@Override
	public void destroy() {
		;
	}
}
