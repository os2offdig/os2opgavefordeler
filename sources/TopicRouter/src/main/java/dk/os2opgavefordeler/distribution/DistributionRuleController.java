package dk.os2opgavefordeler.distribution;

import dk.os2opgavefordeler.LoggedInUser;
import dk.os2opgavefordeler.distribution.DistributionRuleController.RuleNotFoundException;
import dk.os2opgavefordeler.distribution.dto.CprDistributionRuleFilterDTO;
import dk.os2opgavefordeler.distribution.dto.TextDistributionRuleFilterDTO;
import dk.os2opgavefordeler.logging.AuditLogger;
import dk.os2opgavefordeler.service.ConfigService;
import org.apache.deltaspike.jpa.api.transaction.Transactional;

import com.google.common.collect.Lists;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import javax.persistence.EntityManager;

import dk.os2opgavefordeler.distribution.dto.DistributionRuleFilterDTO;

import dk.os2opgavefordeler.repository.EmploymentRepository;
import dk.os2opgavefordeler.repository.OrgUnitRepository;

import dk.os2opgavefordeler.model.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Optional;

@RequestScoped
@Transactional
public class DistributionRuleController {

    @Inject
    private DistributionRuleRepository ruleRepository;

    @Inject
    private OrgUnitRepository orgUnitRepository;

    @Inject
    private EmploymentRepository employmentRepository;

    @Inject
    private EntityManager entityManager;

    @Inject
    private DistributionRuleFilterFactory filterFactory;

    @Inject
    private AuditLogger auditLogger;

    @Inject
    private ConfigService configService;

    @Inject @LoggedInUser
		private User currentUser;

	/**
	 * Creates a new distribution rule filter
	 *
	 * @param dto DTO object for the distribution rule filter
	 * @throws OrgUnitNotFoundException
	 * @throws RuleNotFoundException
	 */
    public void createFilter(DistributionRuleFilterDTO dto) throws
            OrgUnitNotFoundException,
            RuleNotFoundException {

        DistributionRule rule = ruleRepository.findBy(dto.distributionRuleId);

        if (rule == null) {
            throw new RuleNotFoundException("No such rule" + dto.distributionRuleId);
        }

        OrgUnit orgUnit = orgUnitRepository.findBy(dto.assignedOrgId);

        if (orgUnit == null) {
            throw new OrgUnitNotFoundException("Organizational unit not found");
        }

		int lowestPriority = 0;
		for (DistributionRuleFilter distributionRuleFilter : rule.getFilters()) {
			if (distributionRuleFilter.getPriority() > lowestPriority) {
				lowestPriority = distributionRuleFilter.getPriority();
			}
		}

		DistributionRuleFilter newFilter = filterFactory.fromDto(dto);
		newFilter.setPriority(lowestPriority + 1);
		
		rule.addFilter(newFilter);

        ruleRepository.save(rule);
        
        logEvent(rule, dto, orgUnit, LogEntry.CREATE_TYPE); // log event
    }

	/**
	 * Updates the distribution rule filter with the specified filter ID
	 *
	 * @param ruleId ID for the distribution rule
	 * @param filterId ID for the distribution rule filter
	 * @param dto DTO object to use when updating the distribution rule filter
	 * @throws OrgUnitNotFoundException
	 * @throws RuleNotFoundException
	 */
    public void updateFilter(long ruleId, long filterId, DistributionRuleFilterDTO dto) throws
            OrgUnitNotFoundException,
            RuleNotFoundException {
        DistributionRule rule = ruleRepository.findBy(ruleId);

        if (rule == null) {
            throw new RuleNotFoundException("No such rule" + dto.distributionRuleId);
        }

        OrgUnit orgUnit = orgUnitRepository.findBy(dto.assignedOrgId);

        if (orgUnit == null) {
            throw new OrgUnitNotFoundException("Organizational unit not found");
        }

        DistributionRuleFilter filterById = rule.getFilterById(filterId);
        filterById.setName(dto.name);

        Employment e = employmentRepository.findBy(dto.assignedEmployeeId);

	    if (e != null) {
            filterById.setAssignedEmployee(e);
        }

        OrgUnit o = orgUnitRepository.findBy(dto.assignedOrgId);

	    if (o != null) {
            filterById.setAssignedOrg(o);
        }

        if (filterById instanceof CprDistributionRuleFilter) {
            CprDistributionRuleFilter f = (CprDistributionRuleFilter) filterById;
            f.setDays(dto.days);
            f.setMonths(dto.months);
        }
        else if (filterById instanceof TextDistributionRuleFilter) {
            TextDistributionRuleFilter f = (TextDistributionRuleFilter) filterById;
            f.setText(dto.text);
        }

        entityManager.merge(filterById);

        logEvent(rule, dto, orgUnit, LogEntry.UPDATE_TYPE); // log event
    }

	/**
	 * Deletes the distribution rule filter with the specified distribution rule ID and filter ID
	 *
	 * @param distributionRuleId ID for the distribution rule
	 * @param filterId ID for the distribution rule filter
	 */
    public void deleteFilter(long distributionRuleId, long filterId) {
        DistributionRule rule = ruleRepository.findBy(distributionRuleId);

        DistributionRuleFilter filterById = rule.getFilterById(filterId);

        OrgUnit orgUnit = filterById.getAssignedOrg();
        Employment employment = filterById.getAssignedEmployee();

        String dataStr = "";

        if (filterById instanceof CprDistributionRuleFilter) {
            CprDistributionRuleFilter f = (CprDistributionRuleFilter) filterById;
            dataStr = "Navn: " + f.getName() + "; Dage: " + f.getDays() + "; Måneder: " + f.getMonths();
        }
        else if (filterById instanceof TextDistributionRuleFilter) {
            TextDistributionRuleFilter f = (TextDistributionRuleFilter) filterById;
            dataStr = "Navn: " + f.getName() + "; Tekst: " + f.getText();
        }

        if (configService.isAuditLogEnabled()) {
            final String userStr = currentUser.getEmail();
            final String orgUnitStr = orgUnit != null ? orgUnit.getName() + " (" + orgUnit.getBusinessKey() + ")" : "";
            final String employmentStr = employment != null ? employment.getName() + " (" + employment.getInitials() + ")" : "";
            final Municipality municipality = currentUser.getMunicipality();

            // log event
            auditLogger.event(rule.getKle().getNumber(), userStr, LogEntry.DELETE_TYPE, LogEntry.EXTENDED_DISTRIBUTION_TYPE, dataStr, orgUnitStr, employmentStr, municipality);
        }

        filterById.setDistributionRule(null);
        rule.removeFilter(filterById);

        entityManager.remove(filterById);
        DistributionRule savedRule = ruleRepository.save(rule);
        
        ArrayList<DistributionRuleFilter> filters = Lists.newArrayList(savedRule.getFilters());
        Collections.sort(filters, Comparator.comparing(DistributionRuleFilter::getPriority));
        
        int priority = 1;
        for (DistributionRuleFilter distributionRuleFilter : filters) {
			distributionRuleFilter.setPriority(priority);
			priority++;
			entityManager.merge(distributionRuleFilter);
		}
        
        ruleRepository.save(rule);
    }
    
    public void higherPriority(long ruleId, long filterId) throws RuleNotFoundException {
        DistributionRule rule = ruleRepository.findBy(ruleId);

        if (rule == null) {
            throw new RuleNotFoundException("No such rule" + ruleId);
        }

        DistributionRuleFilter filterById = rule.getFilterById(filterId);
        DistributionRuleFilter prevFilter = rule.getFilterByPriority(filterById.getPriority()-1);
        
        if(prevFilter!=null) {
	        int filterPriority= filterById.getPriority();
	        int prevFilterPriority= prevFilter.getPriority();
	        filterById.setPriority(prevFilterPriority);
	        prevFilter.setPriority(filterPriority);
	        
	        entityManager.merge(filterById);
	        entityManager.merge(prevFilter);
        }
        //else do nothing we're at the highest priority
    }
    public void lowerPriority(long ruleId, long filterId) throws RuleNotFoundException {
        DistributionRule rule = ruleRepository.findBy(ruleId);

        if (rule == null) {
            throw new RuleNotFoundException("No such rule" + ruleId);
        }

        DistributionRuleFilter filterById = rule.getFilterById(filterId);
        DistributionRuleFilter nextFilter = rule.getFilterByPriority(filterById.getPriority()+1);
        
        if(nextFilter!=null) {
	        int filterPriority= filterById.getPriority();
	        int nextFilterPriority= nextFilter.getPriority();
	        filterById.setPriority(nextFilterPriority);
	        nextFilter.setPriority(filterPriority);
	        
	        entityManager.merge(filterById);
	        entityManager.merge(nextFilter);
        }
        //else do nothing we're at the lowest priority
    }

    public class RuleNotFoundException extends Exception {
        public RuleNotFoundException(String msg) {
            super(msg);
        }

        public RuleNotFoundException(String msg, Throwable t) {
            super(msg, t);
        }
    }

    public class OrgUnitNotFoundException extends Exception {
        public OrgUnitNotFoundException(String msg) {
            super(msg);
        }

        public OrgUnitNotFoundException(String msg, Throwable t) {
            super(msg, t);
        }
    }

    private void logEvent(DistributionRule rule, DistributionRuleFilterDTO dto, OrgUnit orgUnit, String logType) {
        if (configService.isAuditLogEnabled()) {
            final Employment employment = employmentRepository.findBy(dto.assignedEmployeeId);
            final String userStr = currentUser.getEmail();
            final Municipality municipality = currentUser.getMunicipality();
            final String orgUnitStr = orgUnit.getName() + " (" + orgUnit.getBusinessKey() + ")";
            final String employmentStr = employment != null ? employment.getName() + " (" + employment.getInitials() + ")" : "";

            String dataStr = "";

            if (CprDistributionRuleFilterDTO.FILTER_TYPE.equals(dto.type)) {
                dataStr = "Navn: " + dto.name + "; Dage: " + dto.days + "; Måneder: " + dto.months;
            }
            else if (TextDistributionRuleFilterDTO.FILTER_TYPE.equals(dto.type)) {
                dataStr = "Navn: " + dto.name + "; Tekst: " + dto.text;
            }

            // log event
            auditLogger.event(rule.getKle().getNumber(), userStr, logType, LogEntry.EXTENDED_DISTRIBUTION_TYPE, dataStr, orgUnitStr, employmentStr, municipality);
        }
    }

    public void setConfigService(ConfigService configService) {
        this.configService = configService;
    }
}
