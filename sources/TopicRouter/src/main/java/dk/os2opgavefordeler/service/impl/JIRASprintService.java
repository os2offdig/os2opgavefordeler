package dk.os2opgavefordeler.service.impl;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import org.apache.deltaspike.jpa.api.transaction.Transactional;

import dk.os2opgavefordeler.model.JIRASprint;
import dk.os2opgavefordeler.repository.JIRASprintRepository;

@RequestScoped
@Transactional
public class JIRASprintService {

	@Inject
	private JIRASprintRepository jiraSprintRepository;

	public List<JIRASprint> findAll() {
		return jiraSprintRepository.findAll();
	}

	public JIRASprint save(JIRASprint entity) {
		return jiraSprintRepository.save(entity);
	}

}