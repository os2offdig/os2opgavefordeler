package dk.os2opgavefordeler.service.impl;

import dk.os2opgavefordeler.model.Kle;
import dk.os2opgavefordeler.model.Kle_;
import dk.os2opgavefordeler.service.KleService;
import dk.os2opgavefordeler.service.PersistenceService;
import org.apache.deltaspike.jpa.api.transaction.Transactional;
import org.slf4j.Logger;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.List;
import java.util.Optional;

@RequestScoped
@Transactional
public class KleServiceImpl implements KleService {
	@Inject
	private Logger log;

	@Inject
	PersistenceService persistence;

	/**
	 * Fetches all 'main groups'.
	 * Municipality scoping not needed here since main groups cannot be municipality specific.
	 * @return List of all 'main groups'. Groups with no parent.
	 */
	@Override
	public List<Kle> fetchAllKleMainGroups() {
		List<Kle> result = persistence.criteriaFind(Kle.class,
			(cb, cq, ent) -> cq.where(cb.isNull(ent.get(Kle_.parent)))
		);
		return touchChildren(result);
	}

	@Override
	public Optional<Kle> fetchMainGroup(final String number, long municipalityId) {
		Query query = persistence.getEm().createQuery("SELECT k FROM Kle k WHERE k.number = :number AND " +
				"(k.municipality IS NULL OR k.municipality.id = :municipalityId)");
		query.setParameter("number", number);
		query.setParameter("municipalityId", municipalityId);
		try {
			Kle result = (Kle) query.getSingleResult();
			return Optional.of(result);
		} catch	(Exception e){
			return Optional.empty();
		}
	}

	@Override
	@Transactional
	public void storeAllKleMainGroups(List<Kle> mainGroups) {
		log.info("Persisting new KLE");

		for (Kle mainGroup : mainGroups) {
			try {
				// lookup throws NoResultException if the mainGroup does not exist already
				
				Kle existingMainGroup = getKle(mainGroup.getNumber());
				boolean modified = false;
				
				// see if we have any new groups in the mainGroup
				for (Kle group : mainGroup.getChildren()) {
					Kle existingGroup = null;

					for (Kle eg : existingMainGroup.getChildren()) {
						if (eg.getNumber().equals(group.getNumber())) {
							existingGroup = eg;
							break;
						}
					}
					
					if (existingGroup == null) {
						// it is a new group - so add it to the existingMainGroup as a child, and flag it as dirty

						existingMainGroup.addChild(group);
						modified = true;
					}
					else {
						// if it is not a new group, then check the subjects inside the group, to see if we have any new subjects
						
	 					for (Kle subject : group.getChildren()) {
	 						Kle existingSubject = null;

	 						for (Kle es : existingGroup.getChildren()) {
	 							if (es.getNumber().equals(subject.getNumber())) {
	 								existingSubject = es;
	 								break;
	 							}
	 						}

	 						if (existingSubject == null) {
	 							// it is a new subject - so add it to the existingGroup as a child, and flag it as dirty

	 							existingGroup.addChild(subject);
	 							modified = true;
	 						}
						}
					}					
				}

				if (modified) {
					persistence.persist(existingMainGroup);
				}
			}
			catch (NoResultException ex) {
				// new mainGroup - store it in the DB
				persistence.persist(mainGroup);
			}
		}
	}

	@Override
	public Kle getKle(Long id) {
		Query query = persistence.getEm().createQuery("SELECT k FROM Kle k WHERE k.id = :id");
		query.setParameter("id", id);
		return (Kle) query.getSingleResult();
	}
	
	@Override
	public Kle getKle(String kleNumber) {
		Query query = persistence.getEm().createQuery("SELECT k FROM Kle k WHERE k.number = :kleNumber");
		query.setParameter("kleNumber", kleNumber);
		return (Kle) query.getSingleResult();
	}

	private List<Kle> touchChildren(List<Kle> kle) {
		kle.forEach(k -> touchChildren(k.getChildren()));
		return kle;
	}
}
