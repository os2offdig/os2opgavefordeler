package dk.os2opgavefordeler.orgunit;

import dk.os2opgavefordeler.model.Employment;
import dk.os2opgavefordeler.model.OrgUnit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OrgUnitDTO {
	public String businessKey;
	public String name;
	public String email;
	public String phone;
	public String pNumber;
	public String esdhId;
	public String esdhLabel;
	public EmployeeDTO manager;
	public List<EmployeeDTO> employees = new ArrayList<>();
	public List<OrgUnitDTO> children = new ArrayList<>();
	public Map<String, String> foreignKeys = new HashMap<>();

	public OrgUnitDTO() {
		;
	}

	public OrgUnitDTO(String businessKey) {
		this.businessKey = businessKey;
	}

	public OrgUnitDTO(OrgUnit fromOrgUnit, Option includeChildren) {
		if (fromOrgUnit != null) {
			this.businessKey = fromOrgUnit.getBusinessKey();
			this.name = fromOrgUnit.getName();
			this.email = fromOrgUnit.getEmail();
			this.phone = fromOrgUnit.getPhone();
			this.pNumber = fromOrgUnit.getpNumber();
			this.esdhId = fromOrgUnit.getEsdhId();
			this.esdhLabel = fromOrgUnit.getEsdhLabel();
			this.foreignKeys = fromOrgUnit.getForeignKeys();

			Employment manager = fromOrgUnit.getManager().orElse(null);
			this.manager = manager == null ? null : new EmployeeDTO(manager);
			
			if(includeChildren == Option.INCLUDE_CHILDREN) {
				for (Employment empl : fromOrgUnit.getEmployees()) {
					employees.add(new EmployeeDTO(empl));
				}
			}

			if(includeChildren == Option.INCLUDE_CHILDREN){
				for (OrgUnit orgUnit : fromOrgUnit.getChildren()) {
					children.add(new OrgUnitDTO(orgUnit, Option.INCLUDE_CHILDREN));
				}
			}
		} else {
			new OrgUnitDTO();
		}
	}

	@Override
	public String toString() {
		return "OrgUnitDTO{" +
				"businessKey='" + businessKey + '\'' +
				", name='" + name + '\'' +
				", email='" + email + '\'' +
				", phone='" + phone + '\'' +
				", pNumber='" + pNumber + '\'' +
				", esdhId='" + esdhId + '\'' +
				", esdhLabel='" + esdhLabel + '\'' +
				", manager=" + manager +
				", employees=" + employees +
				", children=" + children +
				", foreignKeys=" + foreignKeys +
				'}';
	}

	public enum Option {
		INCLUDE_CHILDREN, DO_NOT_INCLUDE_CHILDREN
	}
}
