package dk.os2opgavefordeler.repository;

import org.apache.deltaspike.data.api.AbstractEntityRepository;
import org.apache.deltaspike.data.api.Repository;
import org.apache.deltaspike.jpa.api.transaction.Transactional;

import dk.os2opgavefordeler.model.JIRASprint;

@Repository(forEntity = JIRASprint.class)
@Transactional
public abstract class JIRASprintRepository extends AbstractEntityRepository<JIRASprint, Long> {

}