package dk.os2opgavefordeler.task;

import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.deltaspike.core.util.StringUtils;
import org.apache.deltaspike.scheduler.api.Scheduled;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import dk.os2opgavefordeler.repository.MunicipalityRepository;
import dk.os2opgavefordeler.service.BootstrappingDataProviderSingleton;
import lombok.extern.log4j.Log4j;

// run task every 30 sec 0/30 * * ? * * *
// run task every 3 months * * * ? 1/3 * *
// If we you ever need to change cron expression
// http://www.quartz-scheduler.org/documentation/quartz-2.x/tutorials/crontrigger.html
// https://www.freeformatter.com/cron-expression-generator-quartz.html#
@Log4j
@Scheduled(cronExpression = "0 0 4 3 * ? *", onStartup = true)// every month at 4 am on the 3rd day 0 0 4 3 * ? *
public class KleUpdateTask implements Job {

	@Inject
	private BootstrappingDataProviderSingleton bootstrap;

	@Inject
	private MunicipalityRepository municipalityRepository;

	private String downloadUrl;

	@PostConstruct
	private void init() {
		log.info("Initializing KLE Update Task");
		downloadUrl = System.getenv("KLE_DOWNLOAD_URL");

		if (StringUtils.isEmpty(downloadUrl)) {
			log.info("Kle update URL is not configured.");
		}
		else {
			// run at startup
			try {
				execute(null);
			}
			catch (Exception ex) {
				log.warn("Failed to execute kle sync task", ex);
			}
		}
	}

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		log.info("Running KLE UPDATE Task");
		if (StringUtils.isEmpty(downloadUrl)) {
			log.info("No download URL configured - will not download new KLEs!");
			return;
		}
		
		if (municipalityRepository.findAll().size() == 0) {
			log.warn("Loading KLEs attempted without municipalities existing, call bootstrap API endpoint to initialize data.");
			return;
		}

		File tempFile = null;
		try{
			tempFile = File.createTempFile("kle-update-", ".xml");
			downloadKles(tempFile);
			bootstrap.updateKLE(tempFile);
		}
		catch (Exception e) {
			log.info("Problem saving file.", e);
		}
		finally {
			try {
				tempFile.delete();
			}
			catch (Exception ex) {
				log.warn("Failed to delete XML file");
			}
		}

	}

	public void downloadKles(File tempFile) throws Exception {
		InputStream in = new URL(downloadUrl).openStream();
		Files.copy(in, tempFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
	}

}
