package dk.os2opgavefordeler.task;

import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.deltaspike.core.util.StringUtils;
import org.apache.deltaspike.scheduler.api.Scheduled;
import org.jboss.resteasy.client.jaxrs.BasicAuthentication;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.fasterxml.jackson.databind.ObjectMapper;

import dk.os2opgavefordeler.model.JIRASprint;
import dk.os2opgavefordeler.model.JIRATask;
import dk.os2opgavefordeler.model.presentation.JIRAIssueDTO;
import dk.os2opgavefordeler.model.presentation.JIRAIssuesDTO;
import dk.os2opgavefordeler.model.presentation.JIRASprintDTO;
import dk.os2opgavefordeler.model.presentation.JIRASprintsDTO;
import dk.os2opgavefordeler.service.impl.JIRASprintService;
import lombok.extern.log4j.Log4j;

// run task every 4 hours  : 0 0 0/4 ? * * *
// run task every 4 minutes: 0 0/4 * * * ?
// If we you ever need to change cron expression
// http://www.quartz-scheduler.org/documentation/quartz-2.x/tutorials/crontrigger.html
// https://www.freeformatter.com/cron-expression-generator-quartz.html#
@Log4j
@Scheduled(cronExpression = "0 0 0/4 ? * * *", onStartup = true)
public class GetJIRAFeedTask implements Job {

	private String login;
	private String password;
	private String jiraBaseUrl;
	private String jiraBoardId;

	@Inject
	private JIRASprintService jiraSprintService;
	private Client client;

	@PostConstruct
	private void init() {
		log.info("Initializing JIRA Task");
		login = System.getenv("JIRA_LOGIN");
		password = System.getenv("JIRA_PASSWORD");
		jiraBaseUrl = System.getenv("JIRA_BASEURL");
		jiraBoardId = System.getenv("JIRA_BOARDID");
		
		if (StringUtils.isEmpty(login) || StringUtils.isEmpty(password) || StringUtils.isEmpty(jiraBaseUrl) || StringUtils.isEmpty(jiraBoardId)) {
			log.info("JIRA feed is not configured.");
		}
		else {
			// run at startup
			try {
				execute(null);
			}
			catch (Exception ex) {
				log.warn("Failed to execute jira task", ex);
			}
		}
	}

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		log.info("Running JIRA Task");
		if (StringUtils.isEmpty(login) || StringUtils.isEmpty(password)) {
			log.info("No jira username/password configured - will not pull data from jira!");
			return;
		}

		client = ClientBuilder.newClient();
		client.register(new BasicAuthentication(login, password));

		JIRASprintsDTO sprints = getSprints();
		if (sprints == null) {
			return;
		}

		for (JIRASprintDTO jiraSprint : sprints.getValues()) {
			JIRASprint sprint = new JIRASprint();
			sprint.setId(jiraSprint.getId());
			sprint.setName(jiraSprint.getName());
			sprint.setStartDate(jiraSprint.getStartDate());
			sprint.setEndDate(jiraSprint.getEndDate());
			sprint.setState(jiraSprint.getState());
			sprint.setTasks(new ArrayList<>());

			String issuesURL = jiraBaseUrl + "/sprint/" + jiraSprint.getId() + "/issue?fields=summary";

			JIRAIssuesDTO issues = getIssues(issuesURL);
			if (issues == null) {
				continue; // abort updating this sprint, will try again later
			}

			for (JIRAIssueDTO issueDTO : issues.getIssues()) {
				JIRATask task = new JIRATask();
				task.setId(issueDTO.getId());
				task.setKey(issueDTO.getKey());
				task.setSummary(issueDTO.getFields().getSummary());

				task.setSprint(sprint);
				sprint.getTasks().add(task);
			}

			// if everything went as expected, update the sprint in the database
			jiraSprintService.save(sprint);
		}
	}

	public JIRASprintsDTO getSprints() {
		WebTarget target = client.target(jiraBaseUrl + "/board/" + jiraBoardId + "/sprint");
		Response response = null;

		int tries = 3;
		do {
			try {
				response = target.request().accept(MediaType.APPLICATION_JSON).get(Response.class);
				String jsonData = response.readEntity(String.class);
				if (response.getStatus() == 200) {
					ObjectMapper mapper = new ObjectMapper();
					try {
						return mapper.readValue(jsonData, JIRASprintsDTO.class);
					} catch (Exception ex) {
						log.error("This is unhandled exception", ex);
					}
				} else if (response.getStatus() != 401) {
					log.error("Failed to fetch sprints from jira (" + response.getStatus() + ") - " + jsonData);
					return null;
				}
			} finally {
				try {
					response.close();
				} catch (Exception e) {
					;
				}
			}
		} while (tries-- > 0);

		return null;
	}

	public JIRAIssuesDTO getIssues(String issuesURL) {
		WebTarget target = client.target(issuesURL);
		Response response = null;

		int tries = 3;
		do {
			try {
				response = target.request().accept(MediaType.APPLICATION_JSON).get(Response.class);
				String jsonData = response.readEntity(String.class);
				if (response.getStatus() == 200) {
					ObjectMapper mapper = new ObjectMapper();
					try {
						return mapper.readValue(jsonData, JIRAIssuesDTO.class);
					} catch (Exception ex) {
						log.error("This is unhandled exception", ex);
					}
				} else if (response.getStatus() != 401) {
					log.error("Failed to fetch sprints from jira (" + response.getStatus() + ") - " + jsonData);
					return null;
				}
			} finally {
				try {
					response.close();
				} catch (Exception e) {
					;
				}
			}
		} while (tries-- > 0);

		return null;
	}
}
