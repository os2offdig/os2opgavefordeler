package dk.os2opgavefordeler.rest;

import java.util.List;
import java.util.stream.Collectors;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.httpclient.HttpStatus;
import org.apache.deltaspike.core.util.StringUtils;

import dk.os2opgavefordeler.auth.GuestAllowed;
import dk.os2opgavefordeler.model.JIRASprint;
import dk.os2opgavefordeler.model.JIRASprintState;
import dk.os2opgavefordeler.model.presentation.JIRAFeedDTO;
import dk.os2opgavefordeler.service.impl.JIRASprintService;

@GuestAllowed
@RequestScoped
@Path("/jira")
public class JiraFeedEndpoint {
	@Inject
	private JIRASprintService jiraSprintService;

	@GET
	@Path("/getFeed")
	public Response getJIRAfeed() {
		List<JIRASprint> allSprints = jiraSprintService.findAll();
		List<JIRASprint> activeSprints = allSprints.stream().filter(s -> JIRASprintState.ACTIVE.equals(s.getState())).collect(Collectors.toList());
		List<JIRASprint> closedSprints = allSprints.stream().filter(s -> JIRASprintState.CLOSED.equals(s.getState())).collect(Collectors.toList());
		String jiraBrowseUrl = System.getenv("JIRA_BROWSE");

		if (StringUtils.isEmpty(jiraBrowseUrl)) {
			return Response.status(HttpStatus.SC_BAD_REQUEST).type(MediaType.TEXT_PLAIN).entity("JIRA feed is not configured.").build();
		}

		JIRAFeedDTO jirafeed = new JIRAFeedDTO();
		jirafeed.setActiveSprints(activeSprints);
		jirafeed.setClosedSprints(closedSprints);
		jirafeed.setBrowseUrl(jiraBrowseUrl);

		return Response.status(200).type(MediaType.APPLICATION_JSON).entity(jirafeed).build();
	}
}
