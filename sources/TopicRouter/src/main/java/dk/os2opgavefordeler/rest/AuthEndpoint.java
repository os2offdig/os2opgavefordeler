package dk.os2opgavefordeler.rest;

import dk.os2opgavefordeler.auth.AuthService;
import dk.os2opgavefordeler.auth.GuestAllowed;
import dk.os2opgavefordeler.model.JIRASprint;
import dk.os2opgavefordeler.model.JIRASprintState;
import dk.os2opgavefordeler.model.presentation.SimpleMessage;
import dk.os2opgavefordeler.service.BootstrappingDataProviderSingleton;
import dk.os2opgavefordeler.service.ConfigService;
import dk.os2opgavefordeler.service.impl.JIRASprintService;

import org.jboss.resteasy.annotations.cache.NoCache;
import org.slf4j.Logger;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@GuestAllowed
@RequestScoped
@Path("/")
public class AuthEndpoint {

	@Inject
	private Logger log;

	@Context
	private HttpServletRequest request;

	@Inject
	private ConfigService config;

	@Inject
	private AuthService authService;

	@Inject
	private BootstrappingDataProviderSingleton bootstrap;

	@GET
	@Path("/bootstrap")
	public Response bootstrap() {
		bootstrap.bootstrap();
		return Response.ok().build();
	}

	@GET
	@Path("/bootstrapProd")
	public Response bootstrapProd(@QueryParam(value = "municipalityName") String municipalityName, @QueryParam(value = "apiKey") String apiKey) {
		bootstrap.bootstrapProduction(municipalityName, apiKey);
		return Response.ok().build();
	}

	@GET
	@Path("/logout")
	@Produces(MediaType.APPLICATION_JSON + "; charset=UTF-8")
	public Response logout() {
		log.info("Logging out user");
		authService.logout();
		return Response.temporaryRedirect(URI.create(config.getHomeUrl())).build();
	}

	@GET
	@Path("/rest/login")
	@Produces(MediaType.TEXT_HTML + "; charset=UTF-8")
	@NoCache
	public Response login(@QueryParam(value = "returnUrl") String returnUrl) {
		return Response.temporaryRedirect(URI.create(config.getHomeUrl())).build();
	}
}
