package dk.os2opgavefordeler.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import org.jboss.resteasy.annotations.cache.NoCache;

@Path("/public")
public class HealthEndpoint extends Endpoint {

	@GET
	@Path("/health")
	@NoCache
	public Response healthCheck() {
		return ok();
	}
}
