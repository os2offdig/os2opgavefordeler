package dk.os2opgavefordeler.model.api;

import java.util.Map;

import dk.os2opgavefordeler.model.OrgUnit;

/**
 * Convenience class for an organisational unit result from an API call
 */
public class OrgUnitApiResultPO {
	private EmploymentApiResultPO manager;

	private String businessKey;
	private String name;
	private String esdhId;
	private String esdhName;
	private String email;
	private String phone;
	private Map<String, String> foreignKeys;

	public OrgUnitApiResultPO(OrgUnit from, EmploymentApiResultPO manager) {
		this.manager = manager;

		this.businessKey = from.getBusinessKey();
		this.name = from.getName();
		this.esdhId = from.getEsdhId();
		this.esdhName = from.getEsdhLabel();
		this.email = from.getEmail();
		this.phone = from.getPhone();
		this.foreignKeys = from.getForeignKeys();
	}

	public EmploymentApiResultPO getManager() {
		return manager;
	}
	
	public Map<String, String> getForeignKeys() {
		return foreignKeys;
	}
	
	public void setForeignKeys(Map<String, String> foreignKeys) {
		this.foreignKeys = foreignKeys;
	}

	public String getBusinessKey() {
		return businessKey;
	}

	public String getName() {
		return name;
	}

	public String getEsdhId() {
		return esdhId;
	}

	public String getEsdhName() {
		return esdhName;
	}

	public void setEsdhName(String esdhName) {
		this.esdhName = esdhName;
	}

	public String getEmail() {
		return email;
	}

	public String getPhone() {
		return phone;
	}
}
