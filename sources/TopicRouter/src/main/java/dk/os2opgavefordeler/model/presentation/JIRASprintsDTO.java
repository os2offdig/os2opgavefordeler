package dk.os2opgavefordeler.model.presentation;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class JIRASprintsDTO {
	private List<JIRASprintDTO> values;
}