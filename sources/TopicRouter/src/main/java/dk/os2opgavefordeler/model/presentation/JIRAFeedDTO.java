package dk.os2opgavefordeler.model.presentation;

import java.util.List;

import dk.os2opgavefordeler.model.JIRASprint;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JIRAFeedDTO {
	private List<JIRASprint> activeSprints;
	private List<JIRASprint> closedSprints;
	private String browseUrl;
}
