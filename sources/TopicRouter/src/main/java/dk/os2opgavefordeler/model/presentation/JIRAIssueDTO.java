package dk.os2opgavefordeler.model.presentation;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class JIRAIssueDTO {
	private long id;
	private String key;
	private JIRAFieldsDTO fields;
}