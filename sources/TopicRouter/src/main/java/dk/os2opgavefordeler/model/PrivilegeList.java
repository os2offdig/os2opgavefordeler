package dk.os2opgavefordeler.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "privilegeGroup" })
@XmlRootElement(name = "PrivilegeList", namespace="http://itst.dk/oiosaml/basic_privilege_profile")
public class PrivilegeList {

	public void setPrivilegeGroup(List<PrivilegeList.PrivilegeGroup> privilegeGroup) {
		this.privilegeGroup = privilegeGroup;
	}

	@XmlElement(name = "PrivilegeGroup", required = true)
	protected List<PrivilegeList.PrivilegeGroup> privilegeGroup;

	public List<PrivilegeList.PrivilegeGroup> getPrivilegeGroup() {
		if (privilegeGroup == null) {
			privilegeGroup = new ArrayList<>();
		}

		return this.privilegeGroup;
	}

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "privilege" })
	public static class PrivilegeGroup {

		@XmlElement(name = "Privilege", required = true)
		protected String privilege;

		@XmlAttribute(name = "Scope")
		protected String scope;

		public String getPrivilege() {
			return privilege;
		}

		public void setPrivilege(String privilege) {
			this.privilege = privilege;
		}

		public String getScope() {
			return scope;
		}

		public void setScope(String scope) {
			this.scope = scope;
		}
	}
}