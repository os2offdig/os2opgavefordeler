package dk.os2opgavefordeler.model.presentation;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import dk.os2opgavefordeler.model.JIRASprintState;
import lombok.Getter;
import lombok.Setter;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class JIRASprintDTO {
	private long id;

	private JIRASprintState state;

	private String name;

	private Date startDate;

	private Date endDate;
}