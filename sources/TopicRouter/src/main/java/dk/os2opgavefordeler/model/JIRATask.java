package dk.os2opgavefordeler.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name="jira_task")
public class JIRATask {

	@Id
	@Column
	private long id;
	
	@Column(name = "issue_key")
	private String key;

	@Column
	private String summary;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "jira_sprint_id")
	@JsonBackReference
	private JIRASprint sprint;
}