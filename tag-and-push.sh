#!/bin/bash

###
### first run start.sh with the correct frontend URL configured
### then run this script, with the tag/push-url for the frontend set to the target customer
###
### then perform a docker login before running this script
###

# tag and push frontend
docker tag frontend os2web/os2opgavefordeler:frontend-$(date +'%Y-%m-%d')
docker push os2web/os2opgavefordeler:frontend-$(date +'%Y-%m-%d')

# tag and push backend
docker tag backend os2web/os2opgavefordeler:backend-$(date +'%Y-%m-%d')
docker push os2web/os2opgavefordeler:backend-$(date +'%Y-%m-%d')
