#!/bin/bash

./wait-for-it.sh -t 30 -h mysql -p 3306


function wait_for_server() {
  until `/opt/jboss/wildfly/bin/jboss-cli.sh -c ":read-attribute(name=server-state)" 2> /dev/null | grep -q running`; do
    sleep 1
  done
}

echo "=> Starting WildFly server"
/opt/jboss/wildfly/bin/standalone.sh -b 0.0.0.0 -bmanagement 0.0.0.0 &

echo "=> Waiting for the server to boot"
wait_for_server

echo "=> Configuring JBOSS"
/opt/jboss/wildfly/bin/jboss-cli.sh --file=/tmp/system_properties.cli --connect

echo "=> Deploying TopicRouter"
cp /tmp/TopicRouter.war /opt/jboss/wildfly/standalone/deployments/TopicRouter.war

echo "=> Shutting down WildFly"
/opt/jboss/wildfly/bin/jboss-cli.sh -c ":shutdown"

echo "=> Restarting WildFly"
/opt/jboss/wildfly/bin/standalone.sh -b 0.0.0.0 -bmanagement 0.0.0.0
