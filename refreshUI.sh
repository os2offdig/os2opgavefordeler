#!/bin/bash

# build the frontend
(cd ./sources/frontend/src/main/webapp; gulp clean-build-app-dev)
cp -r ./sources/frontend/src/main/webapp/dist.dev/* ./docker/frontend/files
(cd ./docker/frontend; docker build -t frontend .)

docker-compose up -d frontend
