OS2opgavefordeler was originally developed by Syddjurs Kommune, re-implemented by Miracle A/S, and currently maintained by Digital Identity for OS2 - Offentligt digitaliseringsfællesskab (http://os2.eu).

Copyright (c) 2017, OS2 - Offentligt digitaliseringsfællesskab.

OS2kravmotor is free software; you may use, study, modify and
distribute it under the terms of version 2.0 of the Mozilla Public
License. See the LICENSE file for details. If a copy of the MPL was not
distributed with this file, You can obtain one at
http://mozilla.org/MPL/2.0/.

All source code in this and the underlying directories is subject to
the terms of the Mozilla Public License, v. 2.0. 

